FROM alpine:latest

COPY rootfs/ /
RUN chmod +x /sbin/runit-docker \
 && sed -i 's/\r//g' /sbin/runit-docker \
 && chmod +x /etc/service/api/run \
 && sed -i 's/\r//g' /etc/service/api/run

RUN apk add --no-cache bash ca-certificates runit
EXPOSE 19990
# CMD PORT=3001 npm start
ENTRYPOINT ["/sbin/runit-docker"]
// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"log"
	"net"
	"os"
)

func GetPrimaryInterfaceIP() string {

	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	addr := fmt.Sprintf("%s", localAddr.IP)
	return addr
}

func GetHostMachineName() string {
	hostname, err := os.Hostname()
	if err == nil {
		fmt.Println("hostname:", hostname)
	}
	return hostname
}

func GetUrlAddress() string {
	appIP := os.Getenv("APP_IP")
	if appIP == "" {
		appIP = GetPrimaryInterfaceIP()
	}
	return appIP
}

const randomData = `{
    "Endpoint": {
        "nodes": {
            ";10.56.2.12;57520": {
                "id": ";10.56.2.12;57520",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.160.128;19999"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.13;42548": {
                "id": ";10.56.0.13;42548",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.23;10255"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149604438Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149604438Z",
                        "value": "10557"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.255.1.2;54434": {
                "id": ";10.255.1.2;54434",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.160.128;19999"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.13;60974": {
                "id": ";10.56.2.13;60974",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.3.9;2380"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.053565209Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.053565209Z",
                        "value": "24028"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.2.3;10254": {
                "id": ";10.56.2.3;10254",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.053483598Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.053483598Z",
                        "value": "8791"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.168.0;37425": {
                "id": ";100.64.168.0;37425",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;179"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.781324737Z",
                        "value": "c3r-games-dev-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.781324737Z",
                        "value": "4160"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.0.14;53922": {
                "id": ";10.56.0.14;53922",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.6;2380"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.148682679Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.148682679Z",
                        "value": "16412"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;39102": {
                "id": ";100.64.0.254;39102",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.142;19990"
                ],
                "parents": null,
                "children": null
            },
            ";166.170.23.44;62894": {
                "id": ";166.170.23.44;62894",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.26;3080"
                ],
                "parents": null,
                "children": null
            },
            ";166.170.23.44;40218": {
                "id": ";166.170.23.44;40218",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.26;3080"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.20.1;21576": {
                "id": ";100.64.20.1;21576",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;4040"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.216.5;15509": {
                "id": ";10.22.216.5;15509",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.224.125.245;6783"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.747866898Z",
                        "value": "c3r-gcp-core-02-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.747866898Z",
                        "value": "974"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.26;21579": {
                "id": ";10.0.0.26;21579",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.224.245.122;6783"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.005863574Z",
                        "value": "c3r-gcp-core-03-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.005863574Z",
                        "value": "1136"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.3.9;45218": {
                "id": ";10.56.3.9;45218",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.6;2380"
                ],
                "parents": null,
                "children": null
            },
            ";10.255.101.28;1320": {
                "id": ";10.255.101.28;1320",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";172.217.4.74;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.156640381Z",
                        "value": "c3r-dev-pogue.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.156640381Z",
                        "value": "2172"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;42102": {
                "id": ";100.64.0.254;42102",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";172.17.0.2;51682": {
                "id": ";172.17.0.2;51682",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";172.217.5.234;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.437804691Z",
                        "value": "64064030f3db;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.437804691Z",
                        "value": "1532"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;26402": {
                "id": ";100.64.0.254;26402",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.142;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;51154": {
                "id": ";100.64.0.254;51154",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;4556": {
                "id": ";100.64.0.254;4556",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.24;16486": {
                "id": ";10.0.0.24;16486",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.8;4040"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;8102": {
                "id": ";10.22.217.142;8102",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;46926": {
                "id": ";100.64.0.254;46926",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.1.9;42120": {
                "id": ";10.56.1.9;42120",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.10;8201"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.78994895Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-xktz;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.78994895Z",
                        "value": "84477"
                    }
                },
                "parents": null,
                "children": null
            },
            "c3r-games-dev-01-1.weave.local-4026531993;127.0.0.1;44642": {
                "id": "c3r-games-dev-01-1.weave.local-4026531993;127.0.0.1;44642",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-games-dev-01-1.weave.local-4026531993;127.0.0.1;6062"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.781458649Z",
                        "value": "c3r-games-dev-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.781458649Z",
                        "value": "2470"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.218.2;29096": {
                "id": ";10.22.218.2;29096",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";74.125.129.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.781205923Z",
                        "value": "c3r-games-dev-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.781205923Z",
                        "value": "2704"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.113.18;58810": {
                "id": ";10.56.113.18;58810",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;16324": {
                "id": ";10.22.217.142;16324",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.136;31200"
                ],
                "parents": null,
                "children": null
            },
            ";170.20.11.19;18388": {
                "id": ";170.20.11.19;18388",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";64.30.227.218;33398": {
                "id": ";64.30.227.218;33398",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.88.128;53588": {
                "id": ";100.64.88.128;53588",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19999"
                ],
                "parents": null,
                "children": null
            },
            "c3r-gcp-core-02-1.weave.local-4026531993;127.0.0.1;6061": {
                "id": "c3r-gcp-core-02-1.weave.local-4026531993;127.0.0.1;6061",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.74803401Z",
                        "value": "c3r-gcp-core-02-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.74803401Z",
                        "value": "369"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.255.1.2;60863": {
                "id": ";10.255.1.2;60863",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.172.0;19999"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;35058": {
                "id": ";10.0.0.26;35058",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;3025"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:12.006970574Z",
                        "value": ";100.64.0.254;35058"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.144.21;11293": {
                "id": ";10.0.144.21;11293",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.24.249.245;6783"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.641586615Z",
                        "value": "c3r-amlg-dev-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.641586615Z",
                        "value": "970"
                    }
                },
                "parents": null,
                "children": null
            },
            ";64.30.227.218;49637": {
                "id": ";64.30.227.218;49637",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.3;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.4.2;443": {
                "id": ";10.56.4.2;443",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.907552078Z",
                        "value": "gke-core-services-cl-core-services-no-4a28b4d8-m5vq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.907552078Z",
                        "value": "2675659"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.217.142;6778": {
                "id": ";10.22.217.142;6778",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.16.231.100;52566": {
                "id": ";10.16.231.100;52566",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.216.5;19989"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;40598": {
                "id": ";10.0.0.26;40598",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.24;30460"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.005047628Z",
                        "value": "c3r-gcp-core-03-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.005047628Z",
                        "value": "5555"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.101.110;38714": {
                "id": ";10.0.101.110;38714",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.102.155;31200"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;5042": {
                "id": ";100.64.0.254;5042",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.1;58882": {
                "id": ";10.56.2.1;58882",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.12;9090"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;7106": {
                "id": ";10.22.217.142;7106",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";151.101.64.70;443": {
                "id": ";151.101.64.70;443",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";170.20.11.19;49394": {
                "id": ";170.20.11.19;49394",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";216.58.217.138;443": {
                "id": ";216.58.217.138;443",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.0.254;42874": {
                "id": ";100.64.0.254;42874",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.13;60988": {
                "id": ";10.56.2.13;60988",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.3.9;2380"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.053571199Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.053571199Z",
                        "value": "24028"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;1686": {
                "id": ";100.64.0.254;1686",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.4.15;56716": {
                "id": ";10.56.4.15;56716",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";74.125.202.101;443"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;57574": {
                "id": ";100.64.0.254;57574",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.255.1.2;49889": {
                "id": ";10.255.1.2;49889",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.160.128;19999"
                ],
                "parents": null,
                "children": null
            },
            "c3r-core-03-1.weave.local-4026531993;127.0.0.1;6061": {
                "id": "c3r-core-03-1.weave.local-4026531993;127.0.0.1;6061",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.167687224Z",
                        "value": "c3r-core-03-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.167687224Z",
                        "value": "874"
                    }
                },
                "parents": null,
                "children": null
            },
            ";172.17.0.2;36858": {
                "id": ";172.17.0.2;36858",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";172.217.7.170;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.437914006Z",
                        "value": "64064030f3db;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.437914006Z",
                        "value": "1532"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.25;52796": {
                "id": ";10.0.0.25;52796",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.15;3000"
                ],
                "parents": null,
                "children": null
            },
            ";170.20.11.19;42806": {
                "id": ";170.20.11.19;42806",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";170.20.96.7;8309": {
                "id": ";170.20.96.7;8309",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.3;443"
                ],
                "parents": null,
                "children": null
            },
            "gke-core-services-cl-core-services-no-0b79e49a-f1tf-4026531993;127.0.0.1;49868": {
                "id": "gke-core-services-cl-core-services-no-0b79e49a-f1tf-4026531993;127.0.0.1;49868",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "gke-core-services-cl-core-services-no-0b79e49a-f1tf-4026531993;127.0.0.1;10255"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:05.15001838Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:05.15001838Z",
                        "value": "1797"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;43214": {
                "id": ";100.64.0.254;43214",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";100.24.249.245;36173": {
                "id": ";100.24.249.245;36173",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.218.2;6783"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;59870": {
                "id": ";100.64.0.254;59870",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;4040"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.4.162;80": {
                "id": ";10.56.4.162;80",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.22.217.197;21516": {
                "id": ";10.22.217.197;21516",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.196;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.14;53920": {
                "id": ";10.56.0.14;53920",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.6;2380"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149192819Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149192819Z",
                        "value": "16412"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.21.41.3;46854": {
                "id": ";10.21.41.3;46854",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.43.239;3306"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:07.166372407Z",
                        "value": ";100.64.224.0;46854"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.101.110;13706": {
                "id": ";10.0.101.110;13706",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";216.58.217.74;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:00.425248313Z",
                        "value": "c3r-core-02.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:00.425248313Z",
                        "value": "791"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.23;52480": {
                "id": ";10.0.0.23;52480",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.8;4040",
                    ";10.59.249.63;80"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.054424072Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.054424072Z",
                        "value": "1849"
                    }
                },
                "parents": null,
                "children": null
            },
            "c3r-games-dev-01-1.weave.local;127.0.0.1;6062": {
                "id": "c3r-games-dev-01-1.weave.local;127.0.0.1;6062",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.22.218.2;39130": {
                "id": ";10.22.218.2;39130",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";209.85.147.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.781262372Z",
                        "value": "c3r-games-dev-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.781262372Z",
                        "value": "2704"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.0.1;35044": {
                "id": ";10.56.0.1;35044",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.8;4040"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.101.110;38774": {
                "id": ";10.0.101.110;38774",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.102.155;31200"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;46402": {
                "id": ";100.64.0.254;46402",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;32622": {
                "id": ";10.0.0.26;32622",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.0.0.21;32622": {
                "id": ";10.0.0.21;32622",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.0.0.21;36184": {
                "id": ";10.0.0.21;36184",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.59.240.1;443"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:10.149840442Z",
                        "value": ";10.56.0.3;36184"
                    },
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149030062Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149030062Z",
                        "value": "5423"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;47166": {
                "id": ";100.64.0.254;47166",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.168.0;53046": {
                "id": ";100.64.168.0;53046",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;4040"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;17170": {
                "id": ";10.22.217.197;17170",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.196;19990"
                ],
                "parents": null,
                "children": null
            },
            ";80.169.31.214;15626": {
                "id": ";80.169.31.214;15626",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.113.18;58756": {
                "id": ";10.56.113.18;58756",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.160.128;48232": {
                "id": ";100.64.160.128;48232",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;4040"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.829944712Z",
                        "value": "c3r-demo-central-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.829944712Z",
                        "value": "2915"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.217.197;19272": {
                "id": ";10.22.217.197;19272",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.196;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.4.2;56554": {
                "id": ";10.56.4.2;56554",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.8;4040"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.907558329Z",
                        "value": "gke-core-services-cl-core-services-no-4a28b4d8-m5vq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.907558329Z",
                        "value": "2675659"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.224.0;58752": {
                "id": ";100.64.224.0;58752",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:02.695325963Z",
                        "value": ";10.56.113.18;58752"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.217.142;24562": {
                "id": ";10.22.217.142;24562",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;56828": {
                "id": ";10.0.0.26;56828",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.25;30460"
                ],
                "parents": null,
                "children": null
            },
            ";10.16.113.19;52554": {
                "id": ";10.16.113.19;52554",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.5.133;80"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.23;45588": {
                "id": ";10.0.0.23;45588",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.194.10.240;443"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:12.055190277Z",
                        "value": ";10.56.2.12;45588"
                    },
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.054888155Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.054888155Z",
                        "value": "22148"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.217.142;7920": {
                "id": ";10.22.217.142;7920",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.3.9;38052": {
                "id": ";10.56.3.9;38052",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.6;2380"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.88.128;22190": {
                "id": ";100.64.88.128;22190",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.88.128;22942": {
                "id": ";100.64.88.128;22942",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.88.128;22490": {
                "id": ";100.64.88.128;22490",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.0.253;50722": {
                "id": ";100.64.0.253;50722",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;4040"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.424726592Z",
                        "value": "c3r-core-02.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.424726592Z",
                        "value": "26141"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;1770": {
                "id": ";100.64.0.254;1770",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.1;63820": {
                "id": ";10.56.2.1;63820",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.12;9090"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.10;42720": {
                "id": ";10.56.2.10;42720",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.3.9;2380"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.053881542Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.053881542Z",
                        "value": "18651"
                    }
                },
                "parents": null,
                "children": null
            },
            "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;127.0.0.1;47452": {
                "id": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;127.0.0.1;47452",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;127.0.0.1;10255"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.20.1;19999": {
                "id": ";100.64.20.1;19999",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.22.217.142;9792": {
                "id": ";10.22.217.142;9792",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.16.114.53;56712": {
                "id": ";10.16.114.53;56712",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.117;3306"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.1;51978": {
                "id": ";10.56.0.1;51978",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.12;8443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:05.150126462Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:05.150126462Z",
                        "value": "1096"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;42938": {
                "id": ";100.64.0.254;42938",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;45846": {
                "id": ";100.64.0.254;45846",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;5094": {
                "id": ";100.64.0.254;5094",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;63352": {
                "id": ";100.64.0.254;63352",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.56.2.1;61562": {
                "id": ";10.56.2.1;61562",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.12;9090"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;60454": {
                "id": ";100.64.0.254;60454",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;20580": {
                "id": ";10.22.217.197;20580",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.196;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.21.41.3;31267": {
                "id": ";10.21.41.3;31267",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.239.72.161;6783"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.167586926Z",
                        "value": "c3r-core-03-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.167586926Z",
                        "value": "1033"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.88.128;56718": {
                "id": ";100.64.88.128;56718",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;19999"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.3.9;60708": {
                "id": ";10.56.3.9;60708",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.10;2380"
                ],
                "parents": null,
                "children": null
            },
            ";170.20.11.19;32395": {
                "id": ";170.20.11.19;32395",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;443"
                ],
                "parents": null,
                "children": null
            },
            "c3r-games-tools-1.weave.local;172.17.0.1;62510": {
                "id": "c3r-games-tools-1.weave.local;172.17.0.1;62510",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-games-tools-1.weave.local;172.17.0.1;53"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;37598": {
                "id": ";100.64.0.254;37598",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            "c3r-core-02.weave.local;127.0.0.1;15394": {
                "id": "c3r-core-02.weave.local;127.0.0.1;15394",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-core-02.weave.local;127.0.0.1;6784"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;9382": {
                "id": ";10.22.217.142;9382",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.211;31200"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;43236": {
                "id": ";100.64.0.254;43236",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.25.15.71;80": {
                "id": ";10.25.15.71;80",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.0.254;16202": {
                "id": ";100.64.0.254;16202",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            "c3r-core-03-1.weave.local-4026531993;127.0.0.1;45240": {
                "id": "c3r-core-03-1.weave.local-4026531993;127.0.0.1;45240",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-core-03-1.weave.local-4026531993;127.0.0.1;6784"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.167398073Z",
                        "value": "c3r-core-03-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.167398073Z",
                        "value": "1033"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;46806": {
                "id": ";100.64.0.254;46806",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.56.0.3;10254": {
                "id": ";10.56.0.3;10254",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149771392Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149771392Z",
                        "value": "5423"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.26;52796": {
                "id": ";10.0.0.26;52796",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.25;30181"
                ],
                "parents": null,
                "children": null
            },
            ";35.184.12.126;48557": {
                "id": ";35.184.12.126;48557",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.216.5;6783"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.21;30460": {
                "id": ";10.0.0.21;30460",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.26;22634"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.144.21;56293": {
                "id": ";10.0.144.21;56293",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.224.125.245;6783"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.641448667Z",
                        "value": "c3r-amlg-dev-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.641448667Z",
                        "value": "970"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;45382": {
                "id": ";100.64.0.254;45382",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;39656": {
                "id": ";100.64.0.254;39656",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";100.24.249.245;43653": {
                "id": ";100.24.249.245;43653",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.175.201;6783"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.1.7;48572": {
                "id": ";10.56.1.7;48572",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.8;8200"
                ],
                "parents": null,
                "children": null
            },
            "gke-core-services-cl-core-services-no-4a28b4d8-m5vq;127.0.0.1;39520": {
                "id": "gke-core-services-cl-core-services-no-4a28b4d8-m5vq;127.0.0.1;39520",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "gke-core-services-cl-core-services-no-4a28b4d8-m5vq;127.0.0.1;10255"
                ],
                "parents": null,
                "children": null
            },
            "c3r-amlg-dev-01-1.weave.local-4026531993;127.0.0.1;6062": {
                "id": "c3r-amlg-dev-01-1.weave.local-4026531993;127.0.0.1;6062",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.641602443Z",
                        "value": "c3r-amlg-dev-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.641602443Z",
                        "value": "28664"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.88.128;52506": {
                "id": ";100.64.88.128;52506",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19999"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.74762943Z",
                        "value": "c3r-gcp-core-02-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.74762943Z",
                        "value": "4953"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.101.110;38914": {
                "id": ";10.0.101.110;38914",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.107.100;31200"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.23;6832": {
                "id": ";10.0.0.23;6832",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.15;3000"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.101.110;38720": {
                "id": ";10.0.101.110;38720",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.102.155;31200"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;6726": {
                "id": ";10.22.217.142;6726",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.25.15.115;80": {
                "id": ";10.25.15.115;80",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";35.184.59.30;6783": {
                "id": ";35.184.59.30;6783",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.0.0.21;61044": {
                "id": ";10.0.0.21;61044",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.162;80"
                ],
                "parents": null,
                "children": null
            },
            ";10.255.101.28;41082": {
                "id": ";10.255.101.28;41082",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";172.217.8.170;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.15639414Z",
                        "value": "c3r-dev-pogue.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.15639414Z",
                        "value": "13137"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.216.5;21209": {
                "id": ";10.22.216.5;21209",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.192.16.163;6783"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.747691622Z",
                        "value": "c3r-gcp-core-02-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.747691622Z",
                        "value": "974"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.175.201;25592": {
                "id": ";10.0.175.201;25592",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";74.125.202.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.702943062Z",
                        "value": "c3r-amlg-prod-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.702943062Z",
                        "value": "14317"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;25822": {
                "id": ";100.64.0.254;25822",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.13;52880": {
                "id": ";10.56.2.13;52880",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.6;2380"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.110.0;62706": {
                "id": ";100.64.110.0;62706",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;3025"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.224.0;46098": {
                "id": ";100.64.224.0;46098",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.71;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:04.694692928Z",
                        "value": ";10.56.113.18;46098"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.2.1;56384": {
                "id": ";10.56.2.1;56384",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.3;10254"
                ],
                "parents": null,
                "children": null
            },
            "64064030f3db-4026532494;127.0.0.1;6784": {
                "id": "64064030f3db-4026532494;127.0.0.1;6784",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.437883687Z",
                        "value": "64064030f3db;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.437883687Z",
                        "value": "1079"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.59.244.66;2379": {
                "id": ";10.59.244.66;2379",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:12.05527356Z",
                        "value": ";10.56.0.14;2379"
                    },
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.053832282Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.053832282Z",
                        "value": "24028"
                    }
                },
                "parents": null,
                "children": null
            },
            "c3r-games-tools-1.weave.local;127.0.0.1;22218": {
                "id": "c3r-games-tools-1.weave.local;127.0.0.1;22218",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-games-tools-1.weave.local;127.0.0.1;6784"
                ],
                "parents": null,
                "children": null
            },
            ";64.30.233.222;52944": {
                "id": ";64.30.233.222;52944",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;443"
                ],
                "parents": null,
                "children": null
            },
            "gke-core-services-cl-core-services-no-4a28b4d8-m5vq;127.0.0.1;39544": {
                "id": "gke-core-services-cl-core-services-no-4a28b4d8-m5vq;127.0.0.1;39544",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "gke-core-services-cl-core-services-no-4a28b4d8-m5vq;127.0.0.1;10255"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;49766": {
                "id": ";100.64.0.254;49766",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.3.9;50778": {
                "id": ";10.56.3.9;50778",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.13;2380"
                ],
                "parents": null,
                "children": null
            },
            ";104.199.123.215;9959": {
                "id": ";104.199.123.215;9959",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.218.2;6783"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.224.0;53542": {
                "id": ";100.64.224.0;53542",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.87;3306"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:09.697151708Z",
                        "value": ";10.16.114.53;53542"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;63572": {
                "id": ";100.64.0.254;63572",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;42242": {
                "id": ";100.64.0.254;42242",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;37076": {
                "id": ";100.64.0.254;37076",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;3025"
                ],
                "parents": null,
                "children": null
            },
            "c3r-amlg-dev-01-1.weave.local;127.0.0.1;5256": {
                "id": "c3r-amlg-dev-01-1.weave.local;127.0.0.1;5256",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-amlg-dev-01-1.weave.local;127.0.0.1;50051"
                ],
                "parents": null,
                "children": null
            },
            ";172.217.4.74;443": {
                "id": ";172.217.4.74;443",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.0.254;15248": {
                "id": ";100.64.0.254;15248",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.224.0;58770": {
                "id": ";100.64.224.0;58770",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:03.696077115Z",
                        "value": ";10.56.113.18;58770"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.224.0;58774": {
                "id": ";100.64.224.0;58774",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:04.694684357Z",
                        "value": ";10.56.113.18;58774"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.101.110;55042": {
                "id": ";10.0.101.110;55042",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.99.241;31200"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.160.128;10618": {
                "id": ";100.64.160.128;10618",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;4040"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.829702318Z",
                        "value": "c3r-demo-central-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.829702318Z",
                        "value": "2915"
                    }
                },
                "parents": null,
                "children": null
            },
            ";166.170.23.44;63004": {
                "id": ";166.170.23.44;63004",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.142;6783"
                ],
                "parents": null,
                "children": null
            },
            ";104.199.123.215;40857": {
                "id": ";104.199.123.215;40857",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.101.110;6783"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;52134": {
                "id": ";10.0.0.26;52134",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.25;30460"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.22;11826": {
                "id": ";10.0.0.22;11826",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.162;80"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.104.128;3022": {
                "id": ";100.64.104.128;3022",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.0.254;50082": {
                "id": ";100.64.0.254;50082",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.104.128;57972": {
                "id": ";100.64.104.128;57972",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;4040"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.209026472Z",
                        "value": "c3r-demo-west-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.209026472Z",
                        "value": "5225"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.255.1.2;64001": {
                "id": ";10.255.1.2;64001",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.172.0;19999"
                ],
                "parents": null,
                "children": null
            },
            "c3r-core-02.weave.local;172.17.0.1;53": {
                "id": "c3r-core-02.weave.local;172.17.0.1;53",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.0.0.26;33548": {
                "id": ";10.0.0.26;33548",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.21;30181"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.22;61878": {
                "id": ";10.0.0.22;61878",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.15;3000"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.1.1;51678": {
                "id": ";10.56.1.1;51678",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;10254"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;8926": {
                "id": ";10.22.217.142;8926",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            "c3r-amlg-dev-01-1.weave.local-4026531993;127.0.0.1;6784": {
                "id": "c3r-amlg-dev-01-1.weave.local-4026531993;127.0.0.1;6784",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.641477513Z",
                        "value": "c3r-amlg-dev-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.641477513Z",
                        "value": "970"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.21.41.3;33752": {
                "id": ";10.21.41.3;33752",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";74.125.69.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.167636484Z",
                        "value": "c3r-core-03-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.167636484Z",
                        "value": "1731"
                    }
                },
                "parents": null,
                "children": null
            },
            ";64.30.227.218;52099": {
                "id": ";64.30.227.218;52099",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.1.7;48664": {
                "id": ";10.56.1.7;48664",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.8;8200"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;27216": {
                "id": ";10.0.0.26;27216",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.25;32622"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.22;32622": {
                "id": ";10.0.0.22;32622",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.0.0.23;45994": {
                "id": ";10.0.0.23;45994",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19999"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:04.060500176Z",
                        "value": ";10.56.2.12;45994"
                    }
                },
                "parents": null,
                "children": null
            },
            ";35.232.2.77;47196": {
                "id": ";35.232.2.77;47196",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.23;22"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;7370": {
                "id": ";100.64.0.254;7370",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.3.9;38130": {
                "id": ";10.56.3.9;38130",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.6;2380"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.24;49668": {
                "id": ";10.0.0.24;49668",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.8;4040"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.1.7;48406": {
                "id": ";10.56.1.7;48406",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.8;8200"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.14;39030": {
                "id": ";10.56.0.14;39030",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.13;2380"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.224.0;58822": {
                "id": ";100.64.224.0;58822",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:07.692309897Z",
                        "value": ";10.56.113.18;58822"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;57126": {
                "id": ";100.64.0.254;57126",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.10;53128": {
                "id": ";10.56.2.10;53128",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.6;2380"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;56800": {
                "id": ";100.64.0.254;56800",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.16.231.100;51917": {
                "id": ";10.16.231.100;51917",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.142;19999"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;20204": {
                "id": ";100.64.0.254;20204",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.142;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;25666": {
                "id": ";10.22.217.197;25666",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.148;31200"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.88.128;22360": {
                "id": ";100.64.88.128;22360",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            "c3r-demo-central-1.weave.local-4026531993;127.0.0.1;22530": {
                "id": "c3r-demo-central-1.weave.local-4026531993;127.0.0.1;22530",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-demo-central-1.weave.local-4026531993;127.0.0.1;6061"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.830400797Z",
                        "value": "c3r-demo-central-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.830400797Z",
                        "value": "1026"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.23;43152": {
                "id": ";10.0.0.23;43152",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.194.10.240;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.05440807Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.05440807Z",
                        "value": "1197"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.3.9;60874": {
                "id": ";10.56.3.9;60874",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.10;2380"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;9959": {
                "id": ";10.22.217.197;9959",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.224.125.245;6783"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.208690695Z",
                        "value": "c3r-demo-west-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.208690695Z",
                        "value": "1515"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.4.162;54604": {
                "id": ";10.56.4.162;54604",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";104.198.250.145;3307"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.2;2528": {
                "id": ";10.22.217.2;2528",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";74.125.132.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.787378782Z",
                        "value": "c3r-games-tools-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.787378782Z",
                        "value": "528"
                    }
                },
                "parents": null,
                "children": null
            },
            ";74.125.126.128;443": {
                "id": ";74.125.126.128;443",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";64.30.233.222;55255": {
                "id": ";64.30.233.222;55255",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.21.41.3;36178": {
                "id": ";10.21.41.3;36178",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.5.133;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:29:56.166844906Z",
                        "value": ";100.64.224.0;36178"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.1.6;49434": {
                "id": ";10.56.1.6;49434",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.10;2380"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.789556985Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-xktz;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.789556985Z",
                        "value": "8665"
                    }
                },
                "parents": null,
                "children": null
            },
            "c3r-core-02.weave.local;172.17.0.1;8470": {
                "id": "c3r-core-02.weave.local;172.17.0.1;8470",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-core-02.weave.local;172.17.0.1;53"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.224.0;46856": {
                "id": ";100.64.224.0;46856",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.43.239;3306"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:07.692321833Z",
                        "value": ";10.16.114.53;46856"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.23;10255": {
                "id": ";10.0.0.23;10255",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.054536922Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.054536922Z",
                        "value": "1167"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;42802": {
                "id": ";100.64.0.254;42802",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.13;42976": {
                "id": ";10.56.0.13;42976",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.21;9100"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149370889Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149370889Z",
                        "value": "10557"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.0.1;40712": {
                "id": ";10.56.0.1;40712",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.8;4040"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.88.128;23226": {
                "id": ";100.64.88.128;23226",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;42086": {
                "id": ";10.0.0.26;42086",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.21;30181"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.2;33550": {
                "id": ";10.22.217.2;33550",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";169.254.169.254;80"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;1694": {
                "id": ";100.64.0.254;1694",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;6624": {
                "id": ";10.22.217.142;6624",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;19386": {
                "id": ";100.64.0.254;19386",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;64202": {
                "id": ";10.0.0.26;64202",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.22;30181"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;49934": {
                "id": ";100.64.0.254;49934",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;51588": {
                "id": ";10.0.0.26;51588",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.25;30181"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.88.128;55814": {
                "id": ";100.64.88.128;55814",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;32622"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.9;8080": {
                "id": ";10.56.2.9;8080",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";107.77.192.141;8240": {
                "id": ";107.77.192.141;8240",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.3;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;25596": {
                "id": ";10.22.217.197;25596",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.132;31200"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;8344": {
                "id": ";10.22.217.142;8344",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.211;31200"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.104.128;26903": {
                "id": ";100.64.104.128;26903",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.20.1;179"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.209185812Z",
                        "value": "c3r-demo-west-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.209185812Z",
                        "value": "794"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.224.0;17481": {
                "id": ";100.64.224.0;17481",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;179"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.703643828Z",
                        "value": "c3r-amlg-prod-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.703643828Z",
                        "value": "3071"
                    }
                },
                "parents": null,
                "children": null
            },
            "gke-core-services-cl-core-services-no-0b79e49a-xktz;127.0.0.1;42688": {
                "id": "gke-core-services-cl-core-services-no-0b79e49a-xktz;127.0.0.1;42688",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "gke-core-services-cl-core-services-no-0b79e49a-xktz;127.0.0.1;10255"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;7564": {
                "id": ";100.64.0.254;7564",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.132;31200": {
                "id": ";10.22.217.132;31200",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            "c3r-amlg-dev-01-1.weave.local;127.0.0.1;6784": {
                "id": "c3r-amlg-dev-01-1.weave.local;127.0.0.1;6784",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.56.113.18;58736": {
                "id": ";10.56.113.18;58736",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.224.0;58814": {
                "id": ";100.64.224.0;58814",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:06.694749938Z",
                        "value": ";10.56.113.18;58814"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.217.142;6700": {
                "id": ";10.22.217.142;6700",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;32446": {
                "id": ";10.22.217.197;32446",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.196;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;39542": {
                "id": ";10.0.0.26;39542",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.21;30181"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;52276": {
                "id": ";10.22.217.197;52276",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.211;31200"
                ],
                "parents": null,
                "children": null
            },
            "c3r-core-02.weave.local-4026531993;127.0.0.1;1736": {
                "id": "c3r-core-02.weave.local-4026531993;127.0.0.1;1736",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-core-02.weave.local-4026531993;127.0.0.1;6784"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.424460535Z",
                        "value": "c3r-core-02.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.424460535Z",
                        "value": "26141"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.175.201;13266": {
                "id": ";10.0.175.201;13266",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";209.85.147.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.703620347Z",
                        "value": "c3r-amlg-prod-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.703620347Z",
                        "value": "1756"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.3.9;60812": {
                "id": ";10.56.3.9;60812",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.10;2380"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;1168": {
                "id": ";10.0.0.26;1168",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.21;31038"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;19990": {
                "id": ";10.22.217.197;19990",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.56.113.18;58786": {
                "id": ";10.56.113.18;58786",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;7894": {
                "id": ";10.22.217.142;7894",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;20996": {
                "id": ";10.0.0.26;20996",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.25;32622"
                ],
                "parents": null,
                "children": null
            },
            ";10.21.41.3;58800": {
                "id": ";10.21.41.3;58800",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:06.169065398Z",
                        "value": ";100.64.224.0;58800"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.21.41.3;39978": {
                "id": ";10.21.41.3;39978",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.5.134;3306"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:10.168162674Z",
                        "value": ";100.64.224.0;39978"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.217.142;60376": {
                "id": ";10.22.217.142;60376",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";169.254.169.254;80"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;14830": {
                "id": ";100.64.0.254;14830",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.224.0;58690": {
                "id": ";100.64.224.0;58690",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:29:57.696460317Z",
                        "value": ";10.56.113.18;58690"
                    }
                },
                "parents": null,
                "children": null
            },
            ";170.20.96.7;5418": {
                "id": ";170.20.96.7;5418",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;32268": {
                "id": ";10.22.217.142;32268",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.148;31200"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.22;57640": {
                "id": ";10.0.0.22;57640",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.8;4040"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.22;37072": {
                "id": ";10.0.0.22;37072",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";74.125.124.128;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.1.6;38404": {
                "id": ";10.56.1.6;38404",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.13;2380"
                ],
                "parents": null,
                "children": null
            },
            ";172.17.0.2;45546": {
                "id": ";172.17.0.2;45546",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";172.217.14.106;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.437797908Z",
                        "value": "64064030f3db;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.437797908Z",
                        "value": "30795"
                    }
                },
                "parents": null,
                "children": null
            },
            ";64.30.227.218;56696": {
                "id": ";64.30.227.218;56696",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.3;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.113.18;58834": {
                "id": ";10.56.113.18;58834",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;42234": {
                "id": ";10.0.0.26;42234",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";173.194.197.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.005228762Z",
                        "value": "c3r-gcp-core-03-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.005228762Z",
                        "value": "1887"
                    }
                },
                "parents": null,
                "children": null
            },
            ";64.30.233.222;33507": {
                "id": ";64.30.233.222;33507",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.21.41.3;35772": {
                "id": ";10.21.41.3;35772",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.66.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:05.166639327Z",
                        "value": ";100.64.20.1;35772"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.0.1;63482": {
                "id": ";10.56.0.1;63482",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.13;9090"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;32096": {
                "id": ";100.64.0.254;32096",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;4040"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.006010009Z",
                        "value": "c3r-gcp-core-03-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.006010009Z",
                        "value": "589"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.88.128;34660": {
                "id": ";100.64.88.128;34660",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.88.128;56942": {
                "id": ";100.64.88.128;56942",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;19999"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;61916": {
                "id": ";10.22.217.142;61916",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.215;31200"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.21;57872": {
                "id": ";10.0.0.21;57872",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.59.240.1;443"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:10.149820286Z",
                        "value": ";10.56.0.6;57872"
                    },
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149525824Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149525824Z",
                        "value": "4065"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.0.1;56160": {
                "id": ";10.56.0.1;56160",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.8;4040",
                    ";10.59.249.63;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:29:59.152278047Z",
                        "value": ";10.0.0.21;56160"
                    },
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:29:59.151974703Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:29:59.151974703Z",
                        "value": "1797"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;26168": {
                "id": ";100.64.0.254;26168",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;8404": {
                "id": ";100.64.0.254;8404",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;42564": {
                "id": ";100.64.0.254;42564",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.3.9;60680": {
                "id": ";10.56.3.9;60680",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.2.10;2380"
                ],
                "parents": null,
                "children": null
            },
            "c3r-amlg-dev-01-1.weave.local;127.0.0.1;6062": {
                "id": "c3r-amlg-dev-01-1.weave.local;127.0.0.1;6062",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.0.254;18730": {
                "id": ";100.64.0.254;18730",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;36750": {
                "id": ";10.0.0.26;36750",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.25;31038"
                ],
                "parents": null,
                "children": null
            },
            "c3r-dev-pogue.weave.local;127.0.0.1;41768": {
                "id": "c3r-dev-pogue.weave.local;127.0.0.1;41768",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-dev-pogue.weave.local;127.0.0.1;50051"
                ],
                "parents": null,
                "children": null
            },
            "c3r-core-03-1.weave.local;127.0.0.1;62736": {
                "id": "c3r-core-03-1.weave.local;127.0.0.1;62736",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-core-03-1.weave.local;127.0.0.1;6784"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.216.5;40484": {
                "id": ";10.22.216.5;40484",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-gcp-core-02-1.weave.local;172.17.0.1;53"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:09.747210389Z",
                        "value": "c3r-gcp-core-02-1.weave.local;172.17.0.1;40484"
                    }
                },
                "parents": null,
                "children": null
            },
            ";64.30.233.222;43879": {
                "id": ";64.30.233.222;43879",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;4154": {
                "id": ";100.64.0.254;4154",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;42878": {
                "id": ";100.64.0.254;42878",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;25938": {
                "id": ";10.22.217.197;25938",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";74.125.142.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.208989716Z",
                        "value": "c3r-demo-west-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.208989716Z",
                        "value": "605"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.21.41.3;35766": {
                "id": ";10.21.41.3;35766",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.66.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:04.166338378Z",
                        "value": ";100.64.20.1;35766"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.113.18;58712": {
                "id": ";10.56.113.18;58712",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.24;36598": {
                "id": ";10.0.0.24;36598",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.162;80"
                ],
                "parents": null,
                "children": null
            },
            "c3r-amlg-prod-01-1.weave.local;127.0.0.1;6061": {
                "id": "c3r-amlg-prod-01-1.weave.local;127.0.0.1;6061",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.0.254;42104": {
                "id": ";100.64.0.254;42104",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.142;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.142;7478": {
                "id": ";10.22.217.142;7478",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";74.125.202.101;443": {
                "id": ";74.125.202.101;443",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.110.0;61886": {
                "id": ";100.64.110.0;61886",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;4040"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.2;2096": {
                "id": ";10.22.217.2;2096",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";74.125.132.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.787345822Z",
                        "value": "c3r-games-tools-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.787345822Z",
                        "value": "528"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.24;64266": {
                "id": ";10.0.0.24;64266",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.15;3000"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.25;47026": {
                "id": ";10.0.0.25;47026",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.194.10.240;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.14;53926": {
                "id": ";10.56.0.14;53926",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.6;2380"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149346295Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149346295Z",
                        "value": "16412"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.217.197;25662": {
                "id": ";10.22.217.197;25662",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";74.125.142.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.209034457Z",
                        "value": "c3r-demo-west-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.209034457Z",
                        "value": "605"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.2.12;55764": {
                "id": ";10.56.2.12;55764",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.25;9100"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.054867398Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.054867398Z",
                        "value": "22148"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.2.4;8082": {
                "id": ";10.56.2.4;8082",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.054279227Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.054279227Z",
                        "value": "2449"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;57850": {
                "id": ";100.64.0.254;57850",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.20.1;19999"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:01.014213781Z",
                        "value": ";10.0.0.21;57850"
                    }
                },
                "parents": null,
                "children": null
            },
            "gke-core-services-cl-core-services-no-0b79e49a-xktz;127.0.0.1;42650": {
                "id": "gke-core-services-cl-core-services-no-0b79e49a-xktz;127.0.0.1;42650",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "gke-core-services-cl-core-services-no-0b79e49a-xktz;127.0.0.1;10255"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.126.16;59712": {
                "id": ";10.56.126.16;59712",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;57456": {
                "id": ";10.0.0.26;57456",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.24;30181"
                ],
                "parents": null,
                "children": null
            },
            ";10.21.41.3;45980": {
                "id": ";10.21.41.3;45980",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.71;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:29:56.166858091Z",
                        "value": ";100.64.224.0;45980"
                    }
                },
                "parents": null,
                "children": null
            },
            "c3r-games-tools-1.weave.local-4026531993;127.0.0.1;6784": {
                "id": "c3r-games-tools-1.weave.local-4026531993;127.0.0.1;6784",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.787304469Z",
                        "value": "c3r-games-tools-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.787304469Z",
                        "value": "5595"
                    }
                },
                "parents": null,
                "children": null
            },
            ";64.30.227.218;52071": {
                "id": ";64.30.227.218;52071",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.13;55372": {
                "id": ";10.56.0.13;55372",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.10;9410"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.14888431Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.14888431Z",
                        "value": "10557"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.26;63820": {
                "id": ";10.0.0.26;63820",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.23;31038"
                ],
                "parents": null,
                "children": null
            },
            ";172.17.0.2;47114": {
                "id": ";172.17.0.2;47114",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";172.217.14.112;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.437908046Z",
                        "value": "64064030f3db;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.437908046Z",
                        "value": "30795"
                    }
                },
                "parents": null,
                "children": null
            },
            "c3r-amlg-prod-01-1.weave.local;172.17.0.1;53": {
                "id": "c3r-amlg-prod-01-1.weave.local;172.17.0.1;53",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.0.254;42164": {
                "id": ";100.64.0.254;42164",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.23;38008": {
                "id": ";10.0.0.23;38008",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";169.254.169.254;80"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.054382424Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.054382424Z",
                        "value": "1167"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.217.142;9034": {
                "id": ";10.22.217.142;9034",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;26356": {
                "id": ";10.0.0.26;26356",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.24;30460"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;7490": {
                "id": ";10.0.0.26;7490",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.21;31038"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;6512": {
                "id": ";10.0.0.26;6512",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.24;30181"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;29540": {
                "id": ";10.0.0.26;29540",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.26;30181"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;19991": {
                "id": ";10.0.0.26;19991",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            "c3r-amlg-dev-01-1.weave.local;127.0.0.1;9352": {
                "id": "c3r-amlg-dev-01-1.weave.local;127.0.0.1;9352",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-amlg-dev-01-1.weave.local;127.0.0.1;6784"
                ],
                "parents": null,
                "children": null
            },
            "gke-core-services-cl-core-services-no-0b79e49a-f1tf-4026532417;127.0.0.1;54130": {
                "id": "gke-core-services-cl-core-services-no-0b79e49a-f1tf-4026532417;127.0.0.1;54130",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "gke-core-services-cl-core-services-no-0b79e49a-f1tf-4026532417;127.0.0.1;18080"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149744781Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149744781Z",
                        "value": "5423"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;7856": {
                "id": ";100.64.0.254;7856",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.1.6;2380": {
                "id": ";10.56.1.6;2380",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.789776351Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-xktz;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.789776351Z",
                        "value": "8665"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;15454": {
                "id": ";100.64.0.254;15454",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.23;4954": {
                "id": ";10.0.0.23;4954",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.15;3000"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;46744": {
                "id": ";100.64.0.254;46744",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.101.110;11790": {
                "id": ";10.0.101.110;11790",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";172.217.7.234;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.424543147Z",
                        "value": "c3r-core-02.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.424543147Z",
                        "value": "791"
                    }
                },
                "parents": null,
                "children": null
            },
            ";64.30.233.222;41103": {
                "id": ";64.30.233.222;41103",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.4.162;54588": {
                "id": ";10.56.4.162;54588",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";104.198.250.145;3307"
                ],
                "parents": null,
                "children": null
            },
            ";172.17.0.2;56029": {
                "id": ";172.17.0.2;56029",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";104.197.70.205;6783"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.43774683Z",
                        "value": "64064030f3db;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.43774683Z",
                        "value": "1079"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.25;49642": {
                "id": ";10.0.0.25;49642",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.15;3000"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;38892": {
                "id": ";10.0.0.26;38892",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.25;31038"
                ],
                "parents": null,
                "children": null
            },
            ";10.16.114.53;46052": {
                "id": ";10.16.114.53;46052",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.54;3306"
                ],
                "parents": null,
                "children": null
            },
            "gke-core-services-cl-core-services-no-ffbe77c5-f5bq-4026532964;127.0.0.1;47200": {
                "id": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq-4026532964;127.0.0.1;47200",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "gke-core-services-cl-core-services-no-ffbe77c5-f5bq-4026532964;127.0.0.1;2379"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.053914728Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.053914728Z",
                        "value": "18651"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.2.12;52752": {
                "id": ";10.56.2.12;52752",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.23;10255"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.054921185Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.054921185Z",
                        "value": "22148"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.224.0;58710": {
                "id": ";100.64.224.0;58710",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:29:59.700065243Z",
                        "value": ";10.56.113.18;58710"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.25;56826": {
                "id": ";10.0.0.25;56826",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.8;4040"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.24;62208": {
                "id": ";10.0.0.24;62208",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.15;3000"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.224.0;53532": {
                "id": ";100.64.224.0;53532",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.87;3306"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:08.694203866Z",
                        "value": ";10.16.114.53;53532"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.217.197;24892": {
                "id": ";10.22.217.197;24892",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.148;31200"
                ],
                "parents": null,
                "children": null
            },
            ";169.254.169.254;49871": {
                "id": ";169.254.169.254;49871",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.226.0.77;10256"
                ],
                "parents": null,
                "children": null
            },
            ";64.30.227.218;39540": {
                "id": ";64.30.227.218;39540",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.14;44966": {
                "id": ";10.56.0.14;44966",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.3.9;2380"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149725666Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149725666Z",
                        "value": "16412"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.2.13;39392": {
                "id": ";10.56.2.13;39392",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.3.9;2380"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.101.110;55026": {
                "id": ";10.0.101.110;55026",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.99.241;31200"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;11930": {
                "id": ";100.64.0.254;11930",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.101.110;14758": {
                "id": ";10.0.101.110;14758",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";216.58.217.74;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.424572236Z",
                        "value": "c3r-core-02.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.424572236Z",
                        "value": "791"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;43130": {
                "id": ";100.64.0.254;43130",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            "c3r-gcp-core-02-1.weave.local;127.0.0.1;9536": {
                "id": "c3r-gcp-core-02-1.weave.local;127.0.0.1;9536",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    "c3r-gcp-core-02-1.weave.local;127.0.0.1;50051"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.216.5;17366": {
                "id": ";10.22.216.5;17366",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";169.254.169.254;80"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;45034": {
                "id": ";10.0.0.26;45034",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.22;30460"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.10;49178": {
                "id": ";10.56.2.10;49178",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.3.9;2380"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.2;2100": {
                "id": ";10.22.217.2;2100",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";74.125.132.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.787186779Z",
                        "value": "c3r-games-tools-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.787186779Z",
                        "value": "528"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.22.217.142;6756": {
                "id": ";10.22.217.142;6756",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.153;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;16874": {
                "id": ";10.22.217.197;16874",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.196;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.13;9090": {
                "id": ";10.56.0.13;9090",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149433227Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149433227Z",
                        "value": "10557"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.4.1;41786": {
                "id": ";10.56.4.1;41786",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.162;80"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.26;15766": {
                "id": ";10.0.0.26;15766",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.23;30460"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.254;7428": {
                "id": ";100.64.0.254;7428",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.1;30334": {
                "id": ";10.56.0.1;30334",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.8;4040"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.13;35624": {
                "id": ";10.56.2.13;35624",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.14;2380"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.160.128;49181": {
                "id": ";100.64.160.128;49181",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.88.128;179"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.830826101Z",
                        "value": "c3r-demo-central-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.830826101Z",
                        "value": "996"
                    }
                },
                "parents": null,
                "children": null
            },
            ";74.125.202.95;443": {
                "id": ";74.125.202.95;443",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.0.0.27;48874": {
                "id": ";10.0.0.27;48874",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.0.26;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.22.217.197;25084": {
                "id": ";10.22.217.197;25084",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.132;31200"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.224.0;58830": {
                "id": ";100.64.224.0;58830",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.25.15.115;80"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:07.692324546Z",
                        "value": ";10.56.113.18;58830"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.22;4376": {
                "id": ";10.0.0.22;4376",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.15;3000"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.0.253;19991": {
                "id": ";100.64.0.253;19991",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";35.226.0.77;10256": {
                "id": ";35.226.0.77;10256",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";100.64.0.254;11044": {
                "id": ";100.64.0.254;11044",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";104.199.123.215;23697": {
                "id": ";104.199.123.215;23697",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.175.201;6783"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.25;50784": {
                "id": ";10.0.0.25;50784",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.59.240.1;443",
                    ";35.194.10.240;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.90774218Z",
                        "value": "gke-core-services-cl-core-services-no-4a28b4d8-m5vq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.90774218Z",
                        "value": "1763"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.0.10;49640": {
                "id": ";10.56.0.10;49640",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.6;2379",
                    ";10.59.244.66;2379"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.14929684Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.14929684Z",
                        "value": "9273"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.20.1;17593": {
                "id": ";100.64.20.1;17593",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.254;179"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.641289829Z",
                        "value": "c3r-amlg-dev-01-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.641289829Z",
                        "value": "3058"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;8760": {
                "id": ";100.64.0.254;8760",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.22.217.197;19990"
                ],
                "parents": null,
                "children": null
            },
            ";170.20.96.7;33087": {
                "id": ";170.20.96.7;33087",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.2;443"
                ],
                "parents": null,
                "children": null
            },
            ";10.0.0.25;43216": {
                "id": ";10.0.0.25;43216",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";169.254.169.254;80"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:09.907749499Z",
                        "value": "gke-core-services-cl-core-services-no-4a28b4d8-m5vq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:09.907749499Z",
                        "value": "610"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.0.13;47946": {
                "id": ";10.56.0.13;47946",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.2;10254"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.148981844Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.148981844Z",
                        "value": "10557"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.255.1.2;61345": {
                "id": ";10.255.1.2;61345",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.160.128;19999"
                ],
                "parents": null,
                "children": null
            },
            ";100.64.110.0;61323": {
                "id": ";100.64.110.0;61323",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.160.128;19999"
                ],
                "latest": {
                    "copy_of": {
                        "timestamp": "2019-02-02T03:30:11.158642392Z",
                        "value": ";10.255.1.2;61323"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.56.1.9;9410": {
                "id": ";10.56.1.9;9410",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.790007888Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-xktz;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.790007888Z",
                        "value": "87187"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.0.22;9676": {
                "id": ";10.0.0.22;9676",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.4.162;80"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.12;58174": {
                "id": ";10.56.2.12;58174",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.0.10;9102"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.054914528Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.054914528Z",
                        "value": "22148"
                    }
                },
                "parents": null,
                "children": null
            },
            ";166.170.23.44;49694": {
                "id": ";166.170.23.44;49694",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.0.144.21;6783"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.0.14;2380": {
                "id": ";10.56.0.14;2380",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:10.149798726Z",
                        "value": "gke-core-services-cl-core-services-no-0b79e49a-f1tf;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:10.149798726Z",
                        "value": "16412"
                    }
                },
                "parents": null,
                "children": null
            },
            ";100.64.0.254;49654": {
                "id": ";100.64.0.254;49654",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";100.64.0.253;19991"
                ],
                "parents": null,
                "children": null
            },
            ";10.56.2.13;49534": {
                "id": ";10.56.2.13;49534",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";10.56.1.6;2380"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.053513133Z",
                        "value": "gke-core-services-cl-core-services-no-ffbe77c5-f5bq;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.053513133Z",
                        "value": "24028"
                    }
                },
                "parents": null,
                "children": null
            },
            ";10.0.101.110;9109": {
                "id": ";10.0.101.110;9109",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";35.192.16.163;6783"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:12.424699513Z",
                        "value": "c3r-core-02.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:12.424699513Z",
                        "value": "1738"
                    }
                },
                "parents": null,
                "children": null
            },
            ";104.197.70.205;6783": {
                "id": ";104.197.70.205;6783",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "parents": null,
                "children": null
            },
            ";10.22.216.5;16034": {
                "id": ";10.22.216.5;16034",
                "topology": "endpoint",
                "counters": null,
                "sets": null,
                "adjacency": [
                    ";173.194.197.95;443"
                ],
                "latest": {
                    "host_node_id": {
                        "timestamp": "2019-02-02T03:30:11.747853374Z",
                        "value": "c3r-gcp-core-02-1.weave.local;\u003chost\u003e"
                    },
                    "pid": {
                        "timestamp": "2019-02-02T03:30:11.747853374Z",
                        "value": "15525"
                    }
                },
                "parents": null,
                "children": null
            },
        }
    }
}`
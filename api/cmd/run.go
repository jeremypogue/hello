// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Api server listening on port 19990")
		StartAPIServer()
	},
}

func init() {
	rootCmd.AddCommand(runCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// runCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// runCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func StartWebServer() {
	fs := http.FileServer(http.Dir("../public"))
	http.Handle("/", fs)

	log.Println("Listening...")
	http.ListenAndServe(":3000", nil)
}

func StartAPIServer() {
	// go StartWebServer()

	router := mux.NewRouter()
	router.HandleFunc("/ipaddress", GetIpAddress).Methods("GET")
	router.HandleFunc("/hostname", GetMachineName).Methods("GET")
	router.HandleFunc("/serviceip", GetServiceIP).Methods("GET")
	router.HandleFunc("/helloinfo", GetHelloInfo).Methods("GET")
	router.HandleFunc("/randomdata", GetJsonBlob).Methods("GET")
	
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedHeaders:   []string{"X-Requested-With", "Content-Type", "Authorization", "Application", "Accept", "Content-Length", "Accept-encoding", "X-CSRF-Token"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "HEAD", "DELETE", "OPTIONS"},
		Debug:            true,
	})

	handler := c.Handler(router)

	log.Fatal(http.ListenAndServe(":19990", handler))
}

func GetIpAddress(w http.ResponseWriter, r *http.Request) {

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Requested-With, Authorization X-Requested-With")
	}

	// Stop here if its Preflighted OPTIONS request
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		// return
	}

	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		// return
	}

	ipaddr := GetPrimaryInterfaceIP()
	fmt.Println("IP: ", ipaddr)
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(ipaddr))
}

func GetMachineName(w http.ResponseWriter, r *http.Request) {

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Requested-With, Authorization X-Requested-With")
	}

	// Stop here if its Preflighted OPTIONS request
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		// return
	}

	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		// return
	}

	name := GetHostMachineName()
	fmt.Println("Hostname: ", name)
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(name))
}

func GetServiceIP(w http.ResponseWriter, r *http.Request) {

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Requested-With, Authorization X-Requested-With")
	}

	// Stop here if its Preflighted OPTIONS request
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		// return
	}

	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		// return
	}
	var addr string
	address := GetUrlAddress()
	if os.Getenv("PRODUCTION") != "" {
		addr = address + ":3001"
	} else {
		addr = address + ":3000"
	}
	fmt.Println("Service IP: ", addr)
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(addr))
}

// {
// 	"Method": "GET",
// 	"URL": {
// 		"Scheme": "",
// 		"Opaque": "",
// 		"User": null,
// 		"Host": "",
// 		"Path": "/favicon.ico",
// 		"RawQuery": "",
// 		"Fragment": ""
// 	},
// 	"Proto": "HTTP/1.1",
// 	"ProtoMajor": 1,
// 	"ProtoMinor": 1,
// 	"Header": {
// 		"Accept": [
// 			"*/*"
// 		],
// 		"Accept-Encoding": [
// 			"gzip, deflate, sdch"
// 		],
// 		"Accept-Language": [
// 			"en-US,en;q=0.8"
// 		],
// 		"Connection": [
// 			"keep-alive"
// 		],
// 		"Cookie": [
// 			"__utma=72123530.124214442.1438251812.1438251812.1438274470.2; __utmc=72123530; __utmz=72123530.1438251812.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)"
// 		],
// 		"User-Agent": [
// 			"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.99 Safari/537.36"
// 		]
// 	},
// 	"Body": {
// 		"Closer": {
// 			"Reader": null
// 		}
// 	},
// 	"ContentLength": 0,
// 	"TransferEncoding": null,
// 	"Close": false,
// 	"Host": "example.com:8080",
// 	"Form": null,
// 	"PostForm": null,
// 	"MultipartForm": null,
// 	"Trailer": null,
// 	"RemoteAddr": "121.122.88.158:65075",
// 	"RequestURI": "/favicon.ico",
// 	"TLS": null
// }

type HelloInfo struct {
	Provider        string
	ClusterName     string
	Region          string
	PodAddress      string
	ServiceAddress  string
	PodName         string
	SourceAddress   string
	Url             string
	UserAgent       string
	Host            string
	DownstreamApiIP string
	Namespace       string
}

func GetHelloInfo(w http.ResponseWriter, r *http.Request) {

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Requested-With, Authorization X-Requested-With")
	}

	// Stop here if its Preflighted OPTIONS request
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		// return
	}

	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		// return
	}

	ua := r.Header.Get("")

	fmt.Println("header: ", ua)

	address := GetUrlAddress()
	name := GetHostMachineName()
	var ipaddr string
	if os.Getenv("MY_POD_IP") != "" {
		ipaddr = os.Getenv("MY_POD_IP")
	} else {
		ipaddr = GetPrimaryInterfaceIP()
	}
	output := HelloInfo{
		Provider:        os.Getenv("PROVIDER"),
		ClusterName:     os.Getenv("KUBERNETES_CLUSTER_NAME"),
		Region:          os.Getenv("REGION"),
		PodAddress:      ipaddr,
		DownstreamApiIP: os.Getenv("APP_IP"),
		ServiceAddress:  address,
		PodName:         name,
		SourceAddress:   r.RemoteAddr,
		Url:             r.URL.String(),
		UserAgent:       r.Header.Get("User-Agent"),
		Host:            r.Host,
		Namespace:       os.Getenv("NAMESPACE"),
	}

	w.Header().Set("Content-Type", "application/json")
	formattedOutput, err := json.MarshalIndent(output, "", " ")
	if err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
	}
	fmt.Println("Responding to request: \n", string(formattedOutput))
	w.Write(formattedOutput)
}


func GetJsonBlob(w http.ResponseWriter, r *http.Request) {

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Requested-With, Authorization X-Requested-With")
	}

	// Stop here if its Preflighted OPTIONS request
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
		// return
	}

	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		// return
	}



	w.Header().Set("Content-Type", "application/json")
	formattedOutput, err := json.MarshalIndent(randomData, "", " ")
	if err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
	}
	fmt.Println("Responding to request: \n", string("responded to random data request"))
	w.Write(formattedOutput)
}

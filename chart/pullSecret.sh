#!/bin/bash
SECRETNAME=pullsecret
NAMESPACE=default

kubectl -n $NAMESPACE create secret docker-registry $SECRETNAME \
  --docker-server=https://registry.gitlab.com \
  --docker-username=xxx \
  --docker-email=xxx \
  --docker-password=xxx

kubectl -n $NAMESPACE patch serviceaccount default \
  -p "{\"imagePullSecrets\": [{\"name\": \"$SECRETNAME\"}]}"


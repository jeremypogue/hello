SHELL = /bin/bash


.PHONY: help build release local all
.DEFAULT_GOAL := help
# From http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Build go app and docker container locally and push to registry
	touch rootfs/usr/bin/api && rm rootfs/usr/bin/api
	cd api && env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -o ../rootfs/usr/bin/api
	docker login registry.gitlab.com -u jeremypogue -p $(DOCKERPW)
	docker build -t registry.gitlab.com/jeremypogue/hello -f Dockerfile .
	docker push registry.gitlab.com/jeremypogue/hello


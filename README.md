## Hello Test App
* * *
### What is hello

hello is a containerized test app for Kubernetes running a Go Api server configured with an endpoint to return information about the host / pod the app is running on along with some header information.

Built to deploy to Kubernetes clusters to provide visibility into loadbalancing, logging, ingress, testing, etc.

* * * 

### Quick start

hello was designed to be ran as a helm chart, although it can be ran without it. Service IP should be updated in the values.yaml file to reflect an IP that will work for your cluster. Additionally environment variables such as Provider, Region, Kubernetes cluster name can be passed via values.yaml and retured as part of the Api call response.

If using a loadbalancer

```bash
helm install hello chart
export SERVICE_IP=$(kubectl get svc --namespace default hello-chart -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
curl http://$SERVICE_IP:19990/helloinfo
```
If using a node port
```bash
IP=$(kubectl get node -o wide | awk {'print $6'} | grep -v INTERNAL-IP)
PORT=$(kubectl get service hello-chart | grep -v NAME | awk {'print $5'} | cut -d ':' -f 2 | cut -d '/' -f1)
curl http://$IP:$PORT/helloinfo

```